package com.apress.todocloud.service;

import com.apress.todocloud.model.ToDo;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@EnableBinding(Sink.class)
@Log4j2
public class ToDoSink {

//    @StreamListener(Sink.INPUT)
    public void process(ToDo message) {
        log.info("SINK -Message Raceived >>> {}", message);
    }
}
