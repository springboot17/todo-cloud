package com.apress.todocloud.service;

import com.apress.todocloud.model.ToDo;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SenderToDo {

    private final MessageChannel input;

    @Scheduled(fixedRate = 5000L)
    public void send() {
        this.input.send(MessageBuilder.withPayload(new ToDo("ReadA Book", true)).build());
    }
}
