package com.apress.todocloud.service;

import java.time.LocalDateTime;

import com.apress.todocloud.model.ToDo;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.messaging.handler.annotation.SendTo;

@EnableBinding(Processor.class)
@Log4j2
public class ToDoProcessor {

    @StreamListener(Processor.INPUT)
    @SendTo(Processor.OUTPUT)
    public ToDo transformUpper(ToDo message) {
        log.info("Processing >>> {}", message);
        message.setDescription(message.getDescription().toUpperCase());
        message.setCompleted(true);
        message.setModified(LocalDateTime.now());
        log.info("Message Processed >>> {}", message);
        return message;
    }

}
