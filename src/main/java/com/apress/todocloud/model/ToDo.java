package com.apress.todocloud.model;

import java.time.LocalDateTime;
import java.util.UUID;

import lombok.Data;

@Data
public class ToDo {

    private String id;
    private String description;
    private LocalDateTime createdAt;
    private LocalDateTime modified;
    private boolean completed;

    public ToDo() {
        this.id = UUID.randomUUID().toString();
        this.createdAt = LocalDateTime.now();
        this.modified = LocalDateTime.now();
    }

    private ToDo(String description) {
        this();
        this.description = description;
    }

    public ToDo(String description, boolean completed) {
        this(description);
        this.completed = completed;
    }

}
