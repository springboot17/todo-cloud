package com.apress.todocloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
//@EnableScheduling
public class TodoCloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodoCloudApplication.class, args);
    }

}
